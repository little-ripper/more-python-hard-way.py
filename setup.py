from setuptools import find_packages, setup

package = "more python hard way"
version = "0.1"

setup(
    name=package,
    version=version,
    description="How to write efficient python code",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    url="url",
)
