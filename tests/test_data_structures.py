from data_structures import *


def test_push():
    colors = SingleLinkedList()
    colors.push("Pthalo Blue")
    assert colors.count() == 1
    colors.push("Ultramarine Blue")
    colors.push("Red")
    assert colors.count() == 3
    assert colors.count() == 3


def test_shift():
    colors = SingleLinkedList()
    colors.shift("Orange")
    colors.shift("Violet")
    colors.shift("Blue")
    colors.shift("Red")


def test_pop():
    colors = SingleLinkedList()
    colors.push("Magenta")
    colors.push("Alizarin")
    colors.push("Dark Green")
    assert colors.pop() == "Dark Green"
    assert colors.pop() == "Alizarin"
    assert colors.pop() == "Magenta"
    assert colors.pop() == None


def test_unshift():
    colors = SingleLinkedList()
    colors.push("Viridian")
    colors.push("San Green")
    colors.push("Van Dyke")
    assert colors.unshift() == "Viridian"
    assert colors.count() == 2
    assert colors.unshift() == "San Green"
    assert colors.count() == 1
    assert colors.unshift() == "Van Dyke"
    assert colors.unshift() == None


def test_remove():
    colors = SingleLinkedList()
    colors.push("Cobalt")
    colors.push("Zinc White")
    colors.push("Yellow")
    colors.push("Perinone")
    assert colors.remove("Zinc White")
    assert colors.remove("Yellow")
    assert colors.count() == 2


def test_repr():
    colors = SingleLinkedList()
    colors.push("Cobalt")
    colors.push("Zinc White")
    colors.push("Yellow")
    colors.push("Perinone")
    print(colors)
