from data_structures import *


def test_push():
    colors = DoubleLinkedList()
    colors.push(10)
    assert colors.count() == 1
    colors.push(100)
    colors.push(1000)
    assert colors.count() == 3
    assert colors.count() == 3
