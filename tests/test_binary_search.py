from algorithms import *


def test_binary_search_recursive():
    n = 111
    target = 1000
    numbers = list(range(n))
    result = binary_search_recursive(numbers, n, target)
    assert target == result if result else result is None


def test_binary_search():
    n = 111
    target = 1000
    numbers = list(range(n))
    result = binary_search(numbers, n, target)
    assert target == result if result else result is None


def test_suffix_array():
    magic = "abracadabra"
    magic_sa = []
    for i in range(len(magic)):
        magic_sa.append(magic[i:])
    magic_sa = sorted(magic_sa)
    assert "abra" == find_shortest(magic_sa, "abra")
    assert "abracadabra" == find_longest(magic_sa, "abra")
    assert "abra", "abracadabra" == find_all(magic_sa, "abra")
