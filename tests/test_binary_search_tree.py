from algorithms import *


def _setup():
    colors = BSTree()
    colors.set("Cad", "Cadmium Red")
    colors.set("Pthalo", "Pthalo BLue")
    colors.set("Aliz", "Alizarin Crimson")
    colors.set("RedHot", "Chilly peppers")
    colors.set("Grey", "Dorian")
    colors.set("Green", "Weed")
    return colors


def setup_numbers():
    numbers = BSTree()
    numbers.set(8, "8")
    numbers.set(3, "3")
    numbers.set(10, "10")
    numbers.set(1, "1")
    numbers.set(6, "6")
    numbers.set(4, "4")
    numbers.set(7, "7")
    numbers.set(14, "14")
    numbers.set(15, "15")
    return numbers


def test_set():
    colors = BSTree()
    colors.set("Cad", "Cadmium Red")
    colors.set("Pthalo", "Pthalo BLue")
    assert colors.root.key == "Cad"
    assert colors.root.right.key == "Pthalo"
    assert colors.root.left is None
    colors.set("Aliz", "Alizarin Crimson")
    assert colors.root.left.key == "Aliz"


def test_get():
    colors = _setup()
    assert colors.get("Cad") == "Cadmium Red"
    assert colors.get("Green") == "Weed"
    assert colors.get("Yellow   ") is None


def test_info():
    colors = _setup()
    assert "There " in colors.info()


def test_list():
    colors = _setup()
    colors.list()


def test_minimum():
    numbers = setup_numbers()
    minimum = numbers.minimum()
    assert minimum == "1"


def test_maximum():
    numbers = setup_numbers()
    maximum = numbers.maximum()
    assert maximum == "15"


def test_successor():
    numbers = setup_numbers()
    succ = numbers.successor(10)
    assert succ.value == "12"


def test_delete():
    numbers = setup_numbers()
    assert numbers.count() == 9
    node14 = numbers._get(14)
    node15 = numbers._get(15)
    assert node14 == node15.parent
    assert node14.left is None
    # case 1
    numbers.delete(15)
    assert node14.right is None
    # case 2
    # add new child to 14 node
    numbers.set(16, "16")
    assert node14.left is None
    assert node14.right is not None
    node16 = numbers._get(16)
    assert node16.parent == node14
    numbers.delete(14)
    assert node16.parent != node14
    # case 3.1
    numbers.set(9, "9")
    numbers.set(25, "25")
    numbers.set(20, "20")
    numbers.set(28, "28")
    assert numbers.count() == 14
    numbers.delete(10)
    assert node16.parent.value == "8"
    # case 3.2
    numbers.set(27, "27")
    numbers.set(26, "26")
    node8 = numbers._get(8)
    node20 = numbers._get(20)
    node25 = numbers._get(25)
    assert node20.parent == node25
    numbers.delete(16)
    assert node20.parent == node8
