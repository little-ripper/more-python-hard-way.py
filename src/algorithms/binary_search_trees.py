# reference: https://en.wikipedia.org/wiki/Binary_search_tree


class BSTreeNode:
    def __init__(self, key, value, left=None, right=None, parent=None):
        self.key = key
        self.value = value
        self.left = left
        self.right = right
        self.parent = parent

    def __repr__(self):
        return f"{self.key}={self.value}:{self.left}:{self.right}"


class BSTree:
    def __init__(self):
        self.root = None
        self.len = 0

    def set(self, key, value):
        if self.root is None:
            self.root = BSTreeNode(key, value)
            self.len += 1
        else:
            node = self.root
            while node:
                if node.key == key:
                    node.value = value
                    break
                elif key < node.key:
                    if node.left:
                        node = node.left
                    else:
                        node.left = BSTreeNode(key, value, parent=node)
                        self.len += 1
                elif key >= node.key:
                    if node.right:
                        node = node.right
                    else:
                        node.right = BSTreeNode(key, value, parent=node)
                        self.len += 1
                else:
                    assert False, "Should not happen."

    def _get(self, key):
        if self.root is None:
            return None
        else:
            node = self.root
            while node:
                if node.key == key:
                    return node
                elif key < node.key:
                    if node.left:
                        node = node.left
                    else:
                        return None
                elif key >= node.key:
                    if node.right:
                        node = node.right
                    else:
                        return None
                else:
                    assert False, "Should not happen."

    def get(self, key):
        node = self._get(key)
        if node:
            return self._get(key).value
        return None

    def info(self):
        result = "There "
        length = self.len
        if length > 1:
            result += f"are {length} nodes, in this tree root included."
        elif length == 1:
            result += f"is 1 node, in this tree only root."
        else:
            result += "are 0 nodes."
        return result

    def _list(self, node):
        if node:
            self._list(node.left)
            print(node.key, node.value)
            self._list(node.right)

    def list(self):
        self._list(self.root)

    def successor(self, key):
        """the successor of a node x in BST is the node with the smallest key
        greater than x's key."""
        node = self._get(key)
        if node.right:
            return self._minimum(node.right)
        node = node.parent
        while node and node.right:
            node.right = node
            node = node.parent
        return node

    def _minimum(self, node):
        while node.left:
            node = node.left
        return node

    def minimum(self):
        return self._minimum(self.root).value

    def predecessor(self, key):
        node = self._get(key)
        if node.left:
            return self._maximum(node.left)
        node = node.parent
        while node and node.left:
            node.left = node
            node = node.parent
        return node

    def _maximum(self, node):
        while node.right:
            node = node.right
        return node

    def maximum(self):
        return self._maximum(self.root).value

    def delete(self, key):
        d = self._get(key)
        if d.left is None:
            self.shift_nodes(d, d.right)
        elif d.right is None:
            self.shift_nodes(d, d.left)
        else:
            succ = self.successor(key)
            if succ.parent != d:
                self.shift_nodes(succ, succ.right)
                succ.right = d.right
                succ.right.parent = succ
            self.shift_nodes(d, succ)
            succ.left = d.left
            succ.left.parent = succ

    def shift_nodes(self, unode, vnode):
        if unode is None:
            self.root = vnode
        elif unode.parent.left:
            unode.parent.left = vnode
        else:
            unode.parent.right = vnode
        if vnode:
            vnode.parent = unode.parent

    def count(self):
        return self.len
