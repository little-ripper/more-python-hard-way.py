"""reference: https://en.wikipedia.org/wiki/Binary_search_algorithm
"""


def binary_search_recursive(a: list, n: int, target: int) -> int | None:
    l = 0
    h = n - 1
    index = (h - l) // 2
    m = a[index]
    n = index if h % 2 == 0 else index + 1
    if m == target:
        return m
    if n <= 0:
        return None
    if target > m:
        return binary_search_recursive(a[n:], n, target)
    if target < m:
        return binary_search_recursive(a[:n], n + 1, target)


def binary_search(a: list, n: int, target: int) -> int | None:
    l = 0
    r = n - 1
    while l <= r:
        index = (l + r) // 2
        m = a[index]
        if m < target:
            l = index + 1
        elif m > target:
            r = index - 1
        else:
            return m
    return None


def suffix_binary_search(a: list[str], n: int, target: str) -> list[str]:
    l = 0
    r = n - 1
    result = []
    while l <= r:
        index = (l + r) // 2
        m = a[index]
        if m >= target:
            result.append(m)
            r = index - 1
        else:
            l = index + 1
    return result


def find_shortest(a, target) -> str:
    n = len(a)
    result = sorted(suffix_binary_search(a, n, target), key=len)
    return result[1]


def find_longest(a, target) -> str:
    n = len(a)
    result = sorted(suffix_binary_search(a, n, target), key=len)
    return result[-1]


def find_all(a, target) -> str:
    n = len(a)
    result = sorted(suffix_binary_search(a, n, target), key=len)
    return result[1], result[-1]


# suffix arrays
