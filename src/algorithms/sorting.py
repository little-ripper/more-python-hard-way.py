class DoubleLinkedListNode:
    def __init__(self, value, nxt, prev):
        self.value = value
        self.nxt = nxt
        self.prev = prev

    def __repr__(self):
        nval = self.nxt and self.nxt.value or None
        pval = self.prev and self.prev.value or None
        return f"{repr(pval)} <- {self.value} -> {repr(nval)}"


class DoubleLinkedList:
    def __init__(self):
        self.begin = None
        self.end = None
        self.len = 0

    def push(self, obj):
        if self.end:
            value = DoubleLinkedListNode(obj, None, self.end)
            self.end.nxt = value
            self.end = value
        else:
            self.begin = DoubleLinkedListNode(obj, None, None)
            self.end = self.begin
        self.len += 1

    def shift(self, obj):
        if self.begin:
            value = DoubleLinkedListNode(obj, self.begin, None)
            self.begin.prev = value
            self.begin = value
        else:
            self.begin = DoubleLinkedListNode(obj, None, None)
            self.end = self.begin
        self.len += 1

    def count(self):
        return self.len


def bubble_sort(numbers):
    while True:
        is_sorted = True
        node = numbers.begin.nxt
        while node:
            if node.prev.value > node.value:
                node.prev.value, node.value = node.value, node.prev.value
                is_sorted = False
            node = node.nxt
        if is_sorted:
            break


numbers = DoubleLinkedList()
numbers.shift(1000)
numbers.shift(10)
numbers.shift(100000)
numbers.shift(100)
print(numbers)
bubble_sort(numbers)
