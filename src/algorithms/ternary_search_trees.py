class TSTreeNode:
    def __init__(self, key, value, low, eq, high):
        self.key = key
        self.low = low
        self.eq = eq
        self.high = high
        self.value = value


class TSTree:
    def __init__(self):
        self.root = None

    def _get(self, node, keys):
        key = keys[0]
        if key < node.key:
            return self._get(node.low, keys)
        elif key == node.key:
            if len(keys) > 1:
                return self._get(node.eq, keys[1:])
            else:
                return node.value
        else:
            return self._get(node.high, keys)

    def get(self, key):
        keys = [x for x in key]
        return self._get(self.root, keys)

    def _find_shortest(self, node, keys):
        key = keys[0]
        if key < node.key:
            return self._find_shortest(node.low, keys)
        elif key == node.key:
            if len(keys) > 1:
                return self._find_shortest(node.eq, keys[1:])
            else:
                return self._equal(node)
        else:
            return self._find_shortest(node.high, keys)

    def _equal(self, node):
        if node.eq is not None and node.value is None:
            print(f"value {node.value}")
            return self._equal(node.eq)
        print(f"low: {node.low} | eq: {node.eq} | high: {node.high}")
        print(f"value {node.value}")
        return node.value

    def find_shortest(self, key):
        keys = [x for x in key]
        return self._find_shortest(self.root, keys)

    def _set(self, node, keys, value):
        next_key = keys[0]
        if not node:
            node = TSTreeNode(next_key, None, None, None, None)
        if next_key < node.key:
            node.low = self._set(node.low, keys, value)
        elif next_key == node.key:
            if len(keys) > 1:
                node.eq = self._set(node.eq, keys[1:], value)
            else:
                node.value = value
        else:
            node.high = self._set(node.high, keys, value)
        return node

    def set(self, key, value):
        keys = [x for x in key]
        self.root = self._set(self.root, keys, value)


if __name__ == "__main__":
    strings = TSTree()
    strings.set("hello", 100)
    strings.set("apple", 5)
    strings.set("application", 10)
    strings.set("luigi", 1000)
    strings.set("appleego", 15)

    # result = strings.get("hello")
    # print(result)
    result = strings.find_shortest("appl")
    print(result)
    # result = strings.get("application")
    # print(result)
