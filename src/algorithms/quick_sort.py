import random


def quicksort(A, lo, hi):
    if lo >= hi or lo < 0:
        return None

    p = partition(A, lo, hi)
    quicksort(A, lo, p - 1)
    quicksort(A, p + 1, hi)


def partition(A, lo, hi):
    pivot = A[hi]
    i = lo - 1
    for j in range(lo, hi):
        if A[j] <= pivot:
            i += 1
            A[i], A[j] = A[j], A[i]
    i += 1
    A[i], A[hi] = A[hi], A[i]
    return i


# ll = [10, 3, 2, 4, 1, 9]
# print(ll)
# quicksort(ll, 0, 5)
# print(ll)

ll = [random.randint(0, 1000) for _ in range(100000)]
quicksort(ll, 0, len(ll) - 1)
print("Done!")
