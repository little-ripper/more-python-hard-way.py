class QueueNode:
    def __init__(self, value, nxt, prev):
        self.value = value
        self.nxt = nxt
        self.prev = prev

    def __repr__(self):
        nval = self.nxt and self.nxt.value or None
        pval = self.prev and self.prev.value or None
        return f"({repr(nval)} <- {self.value} -> {repr(pval)})"


class Queue:
    def __init__(self):
        self.head = None
        self.tail = None
        self.len = 0

    def push(self, obj):
        if self.tail:
            value = QueueNode(obj, None, self.tail)
            self.tail.nxt = value
            self.tail = value
        else:
            self.top = QueueNode(obj, None, None)
            self.tail = self.top
        self.len += 1

    def pop(self):
        value = self.top.value
        self.top = self.top.nxt
        self.top.prev = None
        self.len -= 1
        return value


numbers = Queue()
numbers.push(10)
numbers.push(100)
numbers.push(1000)
