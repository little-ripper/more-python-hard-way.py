from devtools import debug


class SingleLinkedListNode:
    def __init__(self, value, nxt):
        self.value = value
        self.nxt = nxt

    def __repr__(self):
        nval = self.nxt and self.nxt.value or None
        return f"({self.value} -> {repr(nval)})"


class SingleLinkedList:
    def __init__(self):
        self.begin = None
        self.end = None
        self.len = 0

    def __repr__(self):
        # self.begin = self.end
        escape = "\t"
        num_escape = 1
        result = f"({self.begin.value} ->\n" + escape * num_escape
        while self.begin.nxt:
            num_escape += 1
            self.begin = self.begin.nxt
            result += f"({self.begin.value} ->\n" + escape * num_escape
        last = 1 + num_escape
        result = result[:-last]
        result += f" {self.begin.nxt})"
        return result

    def push(self, obj):
        self.end = SingleLinkedListNode(obj, self.end)
        self.len += 1

    def shift(self, obj):
        value = SingleLinkedListNode(obj, None)
        if not self.begin:
            self.begin = value
            return
        self.end = self.begin
        while self.end.nxt:
            self.end = self.end.nxt
            print(f"self.end: {self.end}, self.begin.nxt: {self.begin.nxt}")
        self.end.nxt = value

    def the_counter(self, result):
        if self.end.nxt is None:
            return result
        else:
            self.end = self.end.nxt
            result += 1
            return self.the_counter(result)

    def count_r(self):
        result = 1
        return self.the_counter(result)

    def count_i(self):
        result = 1
        self.begin = self.end
        while self.begin.nxt:
            result += 1
            self.begin = self.begin.nxt
        return result

    def count(self):
        return self.len

    def pop(self):
        if self.end is not None:
            self.begin = self.end
            self.end = self.end.nxt
            self.len -= 1
            return self.begin.value
        return None

    def unshift(self):
        if self.end is None:
            return None
        self.begin = self.end
        prev = None
        while self.begin.nxt:
            prev = self.begin
            self.begin = self.begin.nxt
        value = self.begin.value
        self.len -= 1
        if prev:
            prev.nxt = None
        else:
            self.end = None
        return value

    def remove(self, obj):
        self.begin = self.end
        prev = None
        while self.begin.nxt:
            if self.begin.value == obj:
                break
            prev = self.begin
            self.begin = self.begin.nxt
        prev.nxt = self.begin.nxt
        self.len -= 1
        return self.len


colors = SingleLinkedList()
colors.shift("Orange")
colors.shift("Violet")
colors.shift("Blue")
colors.shift("Red")
print(colors)
