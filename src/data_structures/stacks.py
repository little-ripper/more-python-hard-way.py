class StackNode:
    def __init__(self, value, nxt):
        self.value = value
        self.nxt = nxt

    def __repr__(self):
        nval = self.nxt and self.nxt.value or None
        return f"({self.value} -> {repr(nval)})"


class Stack:
    def __init__(self):
        self.top = None
        self.len = 0

    def push(self, obj):
        self.top = StackNode(obj, self.top)
        self.len += 1

    def pop(self):
        value = self.top.value
        self.top = self.top.nxt
        self.len -= 1
        return value

    def top(self):
        return self.top.value

    def count(self):
        return self.len


colors = Stack()
colors.push("orange")
colors.push("blue")
colors.push("red")

# colors.pop()
# colors.pop()

# assert colors.count() == 1
