class DoubleLinkedListNode:
    def __init__(self, value, nxt, prev):
        self.value = value
        self.nxt = nxt
        self.prev = prev

    def __repr__(self):
        nval = self.nxt and self.nxt.value or None
        pval = self.prev and self.prev.value or None
        return f"{repr(pval)} <- {self.value} -> {repr(nval)}"


class DoubleLinkedList:
    def __init__(self):
        self.begin = None
        self.end = None
        self.len = 0

    def __repr__(self):
        if self.begin is None:
            return ""
        escape = "\t"
        num_escape = 1
        result = f"({self.begin.value} ->\n" + escape * num_escape
        while self.begin.nxt:
            num_escape += 1
            self.begin = self.begin.nxt
            result += f"({self.begin.value} ->\n" + escape * num_escape
        last = 1 + num_escape
        result = result[:-last]
        result += f" {self.begin.nxt})"
        return result

    def push(self, obj):
        if self.end:
            value = DoubleLinkedListNode(obj, None, self.end)
            # the follow line is gold!!
            # you are changing all the self.end.nxt reference
            # everywhere even in value, indeed:
            # >>> self.begin.nxt is value.prev.nxt
            # True
            self.end.nxt = value  # updating self.begin and value
            # >>> self.begin is self.end
            # True
            self.end = value
            # >>> self.begin is self.end
            # False
        else:
            self.begin = DoubleLinkedListNode(obj, None, None)
            self.end = self.begin
        self.len += 1

    def shift(self, obj):
        if self.begin:
            value = DoubleLinkedListNode(obj, self.begin, None)
            self.begin.prev = value
            self.begin = value
        else:
            self.begin = DoubleLinkedListNode(obj, None, None)
            self.end = self.begin
        self.len += 1

    def count(self):
        return self.len

    def pop(self):
        if self.len == 0:
            return None
        result = self.end.value
        if self.len == 1:
            self.begin = None
            self.end = None
        else:
            self.end = self.end.prev
            self.end.nxt = None
        self.len -= 1
        return result

    def unshift(self):
        if self.len == 0:
            return None
        result = self.begin.value
        if self.len == 1:
            self.begin = None
            self.end = None
        else:
            self.begin = self.begin.nxt
            self.begin.prev = None
        self.len -= 1
        return result


if __name__ == "__main__":
    numbers = DoubleLinkedList()
    numbers.push(10)
    numbers.push(100)
    numbers.push(1000)
    numbers.push(10000)
    numbers.push(100000)
    numbers.unshift()
    numbers.unshift()
    numbers.unshift()
    print(numbers)

    # numbers = DoubleLinkedList()
    # numbers.shift(20)
    # numbers.shift(200)
    # numbers.shift(2000)
    # numbers.shift(20000)
    # numbers.shift(200000)
    # print(numbers)
